begin
  require 'bundler/gem_tasks'

Bundler::GemHelper.install_tasks
rescue LoadError
  puts 'You must `gem install bundler` and `bundle install` to run rake tasks'
end

begin
  require 'rubocop'
  require 'rubocop/rake_task'

    Rake::Task[:rubocop].clear if Rake::Task.task_defined?(:rubocop)
    desc 'Execute rubocop'
      RuboCop::RakeTask.new(:rubocop) do |task|
      task.options = ['--display-cop-names', '--display-style-guide']
      task.fail_on_error = true
    end
  task :default => [:spec, :rubocop]
rescue LoadError => e
  warn "#{e.path} is not available"
  task :default => [:spec]
end

begin
  require 'rspec/core/rake_task'

  RSpec::Core::RakeTask.new(:spec) { |task| task.verbose = false }
  task :test => :spec
rescue LoadError => e
  warn "#{e.path} is not available"
end

begin
require 'rake/testtask'

Rake::TestTask.new(:test) do |t|
  t.libs << 'lib'
  t.libs << 'test'
  t.pattern = 'test/**/*_test.rb'
  t.ruby_opts = ['-r./test/test_helper.rb']
  t.ruby_opts << ' -w' unless ENV['NO_WARN'] == 'true'
  t.verbose = true
end
rescue LoadError => e
  warn "#{e.path} is not available"
end

begin
require 'yard'
namespace :doc do
  YARD::Rake::YardocTask.new do |task|
    task.files   = ['LICENSE.md', 'lib/**/*.rb']
    task.options = ['--markup', 'markdown', '--list-undoc']
  end
end
rescue LoadError => e
  warn "#{e.path} is not available"
end
