# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'delegators/version'

Gem::Specification.new do |spec|

  spec.name    = 'delegators'
  spec.version = Delegators::Version.to_s
  spec.author  = 'Sahal Alarabi'
  spec.email   = 'sahal@menuslate.com'

  spec.summary     = 'delegate it all'
  spec.description = <<-DESCRIPTION
  A gem for delegate attributes and associations of belongs_to and has_one
  DESCRIPTION
  spec.homepage = 'https://gitlab.com/sahal2080/delegators'
  spec.license  = 'JSON'

  spec.platform                  = Gem::Platform::RUBY
  spec.required_ruby_version     = '>= 2'
  spec.required_rubygems_version = Gem::Requirement.new('>= 0') if spec.respond_to? :required_rubygems_version=

  spec.has_rdoc = 'yard'

  spec.files         = Dir['lib/**/*']
  # spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  # spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  rails_versions = ['>= 4.1', '< 6']

  spec.add_dependency 'fast_method_source' #remove later
  spec.add_dependency 'facets'
  spec.add_dependency 'activesupport', rails_versions

  spec.add_runtime_dependency 'dry-types'
  spec.add_runtime_dependency 'dry-initializer'
  spec.add_runtime_dependency 'garage'
  spec.add_runtime_dependency 'refineder'
  spec.add_runtime_dependency 'active_record_auto_build_associations'

  spec.add_runtime_dependency 'activemodel', rails_versions
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'bundler'

  spec.post_install_message = <<-POST_INSTALL_MESSAGE
#{'*' * 80}

  Enjoy!

#{'*' * 80}
  POST_INSTALL_MESSAGE
end
