require 'active_record/auto_build_associations'
require 'garage/association'

module Delegators
  module Associations

    using refined 'garage_core_ext/object/safe_wrap'
    using refined 'active_support/core_ext/module/attribute_accessors'

    protected

    mattr_accessor :taboo_delegate_columns do
      [
          :created_at,
          :created_on,
          :updated_at,
          :updated_on,
          :lock_version,
          :type,
          :id,
          :position,
          :parent_id,
          :lft,
          :rgt
      ]
    end


    #
    # @param [Symbol] association to be delegated
    #
    def delegate_association(delegator_class, association, delegatee)
      delegator_class.include ActiveRecord::AutoBuildAssociations
      assoc_name = association.name
      delegator_class.auto_build_association assoc_name.to_sym
      Garage::Delegation.delegate_accessor(delegator_class, assoc_name, delegatee)

      # _attributes=
      begin
        Garage::Delegation.delegate_reader(delegator_class, "#{assoc_name}_attributes=", delegatee)
      rescue
        true
      end

      # build_
      Garage::Delegation.delegate_reader(delegator_class, "build_#{assoc_name}", delegatee) unless association.collection?
    end


    def associations_to_delegate(target_association_klass, excluded)
      all_target_associations = target_association_klass.reflect_on_all_associations
      excluded ?
          all_target_associations - excluded :
          all_target_associations
    end


    def delegate_scopes(scopes, prefix, association_sym, delegator_class)

      scopes.each do |skope|

        # handle prefix ourselves
        skope = prefix_association(skope, prefix, association_sym) if prefix

        reflection        = Garage::Association.reflection(delegator_class, association_sym)
        association_klass = Garage::Association.association_klass(reflection)

        delegator_class.scope skope, Proc.new do |*args|
          delegator_class.joins(reflection.name).merge(association_klass.send(skope, *args))
        end
      end
    end


    def association_column_names(association_klass, without_taboo_delegate_columns=true)
      begin
        methods = association_klass.column_names
        methods.reject! { |x| taboo_delegate_columns.include?(x.to_s) } if without_taboo_delegate_columns
        return methods
      rescue
        return []
      end
    end


    def place_association_symbols(delegator_class, association_sym)
      reflection        = Garage::Association.reflection(delegator_class, association_sym)
      association_klass = Garage::Association.association_klass(reflection)
      into_a_delegatee(association_klass, { :association_symbol => association_sym })
    end


    private

    def prefix_association(method, prefix, association_sym)
      prefix == true ?
          "#{association_sym}_#{method}" :
          "#{prefix}_#{method}"
    end

  end
end

