require 'fast_method_source'
require 'garage/modules'
module Delegators
  class Includer < Base


    using refined 'facets/module/all_instance_methods'
    using refined 'facets/module/wrap_method'
    using refined 'facets/array/collapse'
    using refined 'garage_core_ext/object/safe_wrap'


    param :targets, type: Coercible::Array # internal

    with default: proc { [] }, type: Coercible::Array do
      option :exclude_entirely
      option :missing_instance_methodz
    end
    with default: proc { false } do
      option :only_immediate
      option :include_protected
      option :include_private
      option :include_class_methods
    end

    Garage::Modules.give self.delegator_class,
                         :unshared_ancestors,
                         :include_modules_of

# Provides
# [:target]
#   The class from which to steal unincluded instance methods
#
# ==== Options
# [:as]
#   The way in which to define methods. Either `:singleton`, ':instance', ':instance',
#   Default: `nil` which defines the methods in the way they were
# [:exclude]
#   Any ancestor (of the class) that we don't need its methods and its ancestors methods
#   Default: `nil`
# [:exclude_entirely]
#   Any module/ancestor (of the class) that we don't need its own and ancestors' methods
#   Default: `nil`
# [:override]
#   Use this to include only the methods defined in the immediate class itself
#   Default: `false`
# [:only_immediate]
#   Use this to include only the methods defined in the immediate class itself
#   Default: `false`
# [:include_private]
#   To include also private and protected methods, or not
#   Default: `false`
# [:include_protected]
#   To include also protected methods or not. Use only when private methods aren't needed
#   Default: `false`
# [:include_class_methods]
#   To include also singleton methods, or not
#   Default: `false`
#
# ==== Caveats
# - Instance methods only
# - Doesn't include methods overridden
#
# ==== Example
# class A
#   def a
#     "I am an 'a'"
#   end
# end
#
# class B
#  include instance_methods_of(A) do
#     def a
#       super + " and not a 'b'"
#     end
#   end
#
#
#   def b
#     "I cannot say: " + a
#   end
# end


    def prepare
      raise 'not doing that yet' if to.size > 1
      @include_protected         = include_private if include_private
      @missing_class_methodz     = [] if include_class_methods
      @protecteds                = [] if include_protected
      @privates                  = [] if include_private
      @starting_instance_mythods = self.delegator_class.all_instance_methods
    end


    def perform
      targets.each do |target|

        if exclude
          unwelcome_generations = exclude.safe_wrap
          warn "But #{unwelcome_generations} ain't family!" unless (target.ancestors & unwelcome_generations).any?
          exclude.concat(unwelcome_generations.map(&:ancestors))
        end

        find_missings(target)

        @missing_instance_methodz = missing_instance_methodz.safe_wrap.collapse.uniq - self.delegator_class.all_instance_methods
        @missing_class_methodz    = missing_class_methodz.flatten.compact.uniq if include_class_methods

        return unless missing_instance_methodz.any? || missing_class_methodz&.any?

        self.delegator_class.include(new_missing_methods_module(target))
      end

    end


    private

    attr_reader :starting_instance_mythods
    attr_accessor :missing_class_methodz,
                  :privates,
                  :protecteds


    def find_missings(target)
      if self.only_immediate
        ancestors = self.delegator_class.ancestors[2]
        missing_instance_methodz.concat(missing_instance_methods(ancestors))
        protecteds.concat(missing_protected_instance_methods(ancestors)) if include_protected
        privates.concat(missing_private_instance_methods(ancestors)) if include_private
        missing_class_methodz.concat(missing_class_methods(ancestors)) if include_class_methods
      else

        missing_instance_methodz << include_modules_of(target)
        missing_classes = unshared_ancestors(target, type: Class)

        return unless missing_classes.any? || missing_instance_methodz.any?


        missing_instance_methodz.concat(missing_classes.map { |klass| klass.instance_methods(false) })
        missing_instance_methodz.concat(missing_classes.map(&:protected_instance_methods)) if include_protected
        missing_instance_methodz.concat(missing_classes.map(&:private_instance_methods)) if include_private

        missing_class_methodz.concat(missing_classes.map(&:methods).safe_wrap.collapse.uniq - methods) if include_class_methods
      end
    end


    def new_missing_methods_module(target)

      # new module can only see internal method variables
      klass          = self
      instance_meths = missing_instance_methodz
      losers         = []

      if missing_class_methodz&.any?
        class_meths  = missing_class_methodz
        class_losers = []
      end

      # @todo care for visibility
      mod = ::Module.new do
        class_meths.each { |myth| klass.send(:eval_source_for, self, myth, target, class_losers, true) } if class_meths
        instance_meths.each { |myth| klass.send(:eval_source_for, self, myth, target, losers, false) } if instance_meths
      end

      @missing_instance_methodz = losers
      @missing_class_methodz    = class_losers if class_meths

      mod
    end


    def eval_source_for(mod, meth, target, losers, is_classy)
      file, line = target.instance_method(meth).source_location
      mod.module_eval(source_for(meth, target, losers, is_classy), file, line)
    end


    def source_for(meth, target, losers, is_classy)
      unbounded = is_classy ? target.method(meth) : target.instance_method(meth)
      begin
        FastMethodSource.source_for(unbounded)
      rescue => e
        logger.debug "LOOKi HERE: #{meth.inspect} because of #{e}"
        losers << meth
        ''
      end
    end

  end
end
