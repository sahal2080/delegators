module Delegators
  #
  # Defines the base `:initialize` for other classes
  #
  class Base
    include Dry::Types.module
    extend Dry::Initializer::Mixin

    param :delegator_class, type: Class # internal

    with default: proc { [] }, type: Coercible::Array do
      option :only # ActiveSupport#delegate
      option :except # ActiveSupport#delegate
      option :exclude
    end
    with default: proc { false } do
      option :allow_nil # ActiveSupport#delegate
      option :associations
      option :association_attributes
      option :missing_instance_methods
    end
    with optional: true do
      option :prefix, optional: true # ActiveSupport#delegate
      option :suffix, optional: true
    end

    option :type, default: proc { 'reader' }, type: Coercible::String
    # internal - used for association attributes @todo expose?
    option :suffixes, default: proc { ["=", "?", "_before_type_cast", "_change", "_changed?", "_was", "_will_change!"] }

  end


end
