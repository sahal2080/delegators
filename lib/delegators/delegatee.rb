module Delegators
  #
  # Representative of a delegatee class.Yep, more separation degrees and, who is this?
  #
  class Delegatee < Base

    using refined 'garage_core_ext/object/safe_wrap'
    using refined 'garage_core_ext/object/missings'
    using refined 'facets/array/collapse'


    # new(delegator_class, class, instance, association_symbol, delegated_hash)
    param :_class, type: Class
    param :instance, default: proc { nil }
    param :association_symbol, default: proc { nil }

    # we assume all methods are either class or instance
    option :methods, default: proc { [] }, type: Coercible::Array


    def why_not
      prepare_methods
      accept_responsibility
    end


    def prepare_methods
      @default_options = {
          :only     => self.only,
          :except   => self.except,
          :instance => self.instance,
      }.freeze
      if self.association_symbol
        @associations           = associations_to_delegate(self._class, self.exclude) if self.associations
        @association_attributes = association_column_names(self._class) if self.association_attributes
      end
      @missing_instance_methods = find_missing_instance_methods if self.missing_instance_methods
    end


    def accept_responsibility
      delegate_passed_methods if self.methods.any?
      delegate_missing_instance_methods if self.missing_instance_methods
      delegate_associations if self.associations
      delegate_association_attributes if self.association_attributes
      self
    end


    protected


    def find_missing_instance_methods
      self.delegator_class.missing_instance_methods(self._class)
    end


    def delegate_passed_methods
      case self.type
      when 'scope'
        return unless self.association_symbol # cuz this delegatee isn't an association
        delegate_scopes(self.methods,
                        self.prefix,
                        self.association_symbol,
                        self.delegator_class)
      else
        opts            = @default_options.dup.update({
                                                          :methods => self.methods,
                                                          :type    => self.type,
                                                      })
        opts[:instance] = true unless self.instance # allows passing an actual instance
        Garage::Delegation.bulk_delegate(self.delegator_class, self._class, opts)
      end
    end


    def delegate_missing_instance_methods
      opts            = @default_options.dup.update({
                                                        :methods => self.missing_instance_methods,
                                                        :type    => 'reader'
                                                    })
      opts[:instance] = true unless self.instance
      Garage::Delegation.bulk_delegate(self.delegator_class, self._class, opts)
    end


    def delegate_associations
      self.associations.each { |association| delegate_association(self.delegator_class,
                                                                  association,
                                                                  self._class) }
    end


    def delegate_association_attributes
      opts = @default_options.dup.update({
                                             :methods  => self.association_attributes,
                                             :suffixes => self.suffixes
                                         })
      Garage::Delegation.bulk_delegate(self.delegator_class, self._class, opts)
    end

  end
end
