require 'garage/type'
require 'garage/method'
require_relative 'delegatee'
=begin
    delegatees = {
        :demo1 => ["lights", "tree"],
        :demo2 => {
            :instance           => TBA,
            :association_symbol => TBA,
            :delegated          => { :methods                  => [],
                                     :associations             => false,
                                     :association_attributes   => false,
                                     :missing_instance_methods => false,
                                     :exclude                  => self.exclude,
                                     :only                     => self.only,
                                     :except                   => self.except, },
        }
    }
=end
module Delegators
  #
  #
  #
  class Vanilla < Base

    param :delegated, type: Coercible::Array # internal

    option :to, default: proc { [] }, type: Coercible::Array
    option :delegatees, default: proc { {} } # internal


    def prepare
      raise "(:to) can't be blank!" unless to.any?

      # == Parse `:to` entries
      # -------------------------

      # all symbols in `:to` are associations
      sym_only_ary = extract_associations_from(self.to) if defined? Garage::Association
      @to          = self.to - sym_only_ary if sym_only_ary&.any?

      instances_ary = extract_instances_from(self.to)
      @to           = self.to - instances_ary if instances_ary.any?

      self.to.each { |klass| into_a_delegatee(klass) }


      # == Convert `:delegated` shortcuts
      # --------------------------------------

      [:missing_instance_methods,
       :association_attributes,
       :associations
      ].each { |shortcut| @delegated = replace_shortcut_in(self.delegated, shortcut) }

      into_all_delegatees({ :delegated => { :methods => self.delegated } })
    end


    def perform
      begin
        delegatees.each do |delegatee_class, delegatees|

          unless valid_delegatee?(delegatee_class)
            raise "#{delegatee_class} unrecognized. Plz review documentation of valid delegatees"
          end

          Delegatee.new(self.delegator_class,
                        delegatee_class,
                        delegatees[:instance],
                        delegatees[:association_symbol],
                        delegatees[:delegated])
              .why_not
        end
      end
    end


    private


    # assigns delegate class while assigning `:instance` if it existed
    #
    # @param [Array] to_ary of delegatees
    #
    # @return [Array] to_ary after replacing any delegatee instances with classes
    #
    def extract_instances_from(to_ary)
      instances_ary = []
      to_ary.select { |x| Garage::Delegation.instance?(x) }.map do |delegatee_instance|
        into_a_delegatee(delegatee_instance.class, { instance: delegatee_instance })
        instances_ary << delegatee_instance
      end
      instances_ary
    end


    def extract_associations_from(ary)
      sym_only_ary = symbols_in(ary)
      sym_only_ary.each(&method(:place_association_symbols).curry.call(self.delegator_class))
    end


    def extract_instance_methods

    end


    # replace delegated _shortcuts_, i.e. :associations, :association_attributes with the real thing
    def replace_shortcut_in(delegated_ary, shortcut_sym)
      if delegated_ary.include?(shortcut_sym)
        into_all_delegatees({ :delegated => { shortcut_sym => true } })
        delegated_ary.delete(shortcut_sym)
      end
      delegated_ary
    end


    def into_all_delegatees(hash)
      delegatees.each_key { |delegatee_class| delegatees[delegatee_class].update(hash) }
    end


    # setup a delegatee entry in the `delegatees` hash
    #
    # @param [Class|Module] delegatee_class
    # @param [Hash] more_info any hashes here will be added under this delegatee
    #
    def into_a_delegatee(delegatee_class, more_info=nil)
      # initial delegatee hash
      delegatees[delegatee_class] = {
          :delegated => {
              :exclude => self.exclude,
              :only    => self.only,
              :except  => self.except,
          },
      } unless delegatees[delegatee_class]
      delegatees[delegatee_class].update(more_info) if more_info
    end


    def valid_delegatee?(delegatee)
      delegatee.is_a?(::Module) || Garage::Delegation.instance?(delegatee)
    end


    def has_associations(ary)
      Garage::Type.only(ary, ::Symbol)
    end


    def symbols_in(ary)
      Garage::Type.only(ary, ::Symbol)
    end

  end
end
