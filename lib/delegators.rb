require 'dry-types'
require 'dry-initializer'
require 'refineder'
require 'garage/option'
require 'garage/delegation'

require_relative 'delegators/version'
require_relative 'delegators/base'

if defined?(ActiveRecord)
  require 'active_record'

  if defined? (Rails)
    module Delegators
      class Railtie < Rails::Railtie
        initializer 'delegators.initialize' do
          # @todo should be config dependent
          ActiveSupport.on_load(:active_record) { ActiveRecord::Base.send :extend, Delegators }
        end
      end
    end
  end
end

module Delegators

  using refined 'garage_core_ext/object/safe_wrap'

  ##
  # Creates methods on object which delegate to a delegate/proxy class.
  #
  # Provides a *delegate* class method to expose target methods or ActiveModel's associations,
  # attributes or even scopes as your own.
  #
  # Primary method of delegation is ActiveSupport#delegate method
  #
  # ==== Arguments & Shortcuts
  # Any method or attribute can be specified. Additionally, these _shortcuts_ exist:
  # [:association_attributes]
  #   `ActiveModel` Only: Exposes the target's (an association) attributes as your own.
  # [:associations]
  #   `ActiveModel` Only: Exposes the target's (an association) associations as your own.
  # [:missing_instance_methods]
  #   Find all methods of the specified target that aren't defined on the model and delegate
  #   them to that target
  #
  # ==== Options
  # [:to] *Required*
  #   The target(s) to which the attributes or methods will be delegated.
  #   Must be either a
  #     - Class name as a [Constant]
  #     - Association name as [Symbol]
  #     - Class instance variable
  # [:type]
  #   Either 'accessor', 'writer', 'reader' or 'scope'
  #   Default: 'reader' i.e. as is
  # [:exclude]
  #   For [:associations] Only: Exclude whats given here from delegation.
  #   Must be the symbol reference of associations.
  # [:except]
  #   For all shortcuts but [:associations]: Delegate all methods/attributes except for whats given here.
  # [:only]
  #   For all shortcuts but [:associations]: Delegate only attributes/methods given here.
  # [:allow_nil]
  #    If the target attributes are not +nil+, or are but respond to their methods, nothing changes
  #    if a target attribute it is +nil+ and does not respond to the delegated method, +nil+ is returned.
  # [:prefix]
  #   Prefixes the attributes with the target name or a custom prefix
  def delegates(*args)
    require_relative 'delegators/vanilla'
    hash = Garage::Option.extract_options(args)
    t    = Vanilla.new(self, args, hash)
    if defined?(::ActiveRecord) && t.symbols_in(t.to).any?
      require_relative 'delegators/addenda/associations' and Vanilla.send(:include, Associations)
    end
    process(t)
  end


  # Quickly delegate an `attr_accessors` to a different class
  def delegates_attr_accessors(*opts)
    opts.safe_wrap << { type: 'accessor' }
    delegates(opts)
  end


  def include_methods_of(*args)
    require_relative 'delegators/includer'
    hash = Garage::Option.extract_options(args)
    t    = Includer.new(self, args, hash)
    process(t)
  end


  private


  def process(t)
    t.prepare
    t.perform
  end

end
